from .base import *

DEBUG = False

ADMINS = (
    ('admin_v2', 'email@mydomain.com'),
)

ALLOWED_HOSTS = ['.educaproject.com']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'educa',
        'USER': 'educa1',
        'PASSWORD': 'educa1',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
